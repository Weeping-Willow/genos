<a href=""><img src="https://avatars3.githubusercontent.com/u/25120430?s=460&u=b5defbf5c0dd484320edcb8c0869831f942e650d&v=4" alt="my Cute Profile Picture"></a>

# Genos

This project is a business media system made with Laravel and Vuejs.

---

## Table of Contents

> If your `README` has a lot of info, section headers might be nice.

- [Installation](#installation)
- [Features](#features)
- [Tests](#team)
- [FAQ](#faq)
- [Sources](#support)
- [License](#license)

---

## Installation

1.git clone this project

2.Have docker and docker-compose installed

3.Run this command in the folder where this readme is located in
```shell
$ docker-compose up -d --build 
```

4.docker-compose up -d

5. go to localhost

---

## Commands
these commands need to be used from root folder

1. brings up and builds all the containers
    ```shell
    $ docker-compose up -d --build 
    ```
      
---

## Features

- Login with jwt 

---

## Tests 

- Login

---

## FAQ

- **How do i Login**
    - email - vitolinseriks@gmail.com
    - password - test
- Why did my docker build failed
    - One of the ports needed was most likely blocked
---

## Sources

- https://dev.to/aschmelyun/the-beauty-of-docker-for-local-laravel-development-13c0
- https://hub.docker.com/_/postgres
- https://vuejs.org/v2/guide/installation.html
- https://laravel.com/docs/7.x
- https://www.youtube.com/watch?v=C1r85Q3BFqQ&list=PLJpBh2VJhy5wPhAmjDB42pkHUnqolqxxq&index=5
- https://jwt-auth.readthedocs.io/en/docs/quick-start/
- https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=FFCCBC
- https://fonts.google.com/?sidebar.open=true&selection.family=Roboto
- https://tablericons.com/
- https://www.pixeltrue.com/free-illustrations
---

##Technologies
 Languages and tools used in this project
 
- PHP
- Laravel
- Docker
- Docker-compose
- Composer
- PostgreSql
- https://github.com/apertureless/vue-breakpoints
- vuejs
- scss
- http://flexboxgrid.com/
---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2015 © <a href="http://fvcproductions.com" target="_blank">FVCproductions</a>.