<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    public function login(Request $request)
    {
        return response()->json(['error' => 'Unauthorized'], 401);
    }
}
