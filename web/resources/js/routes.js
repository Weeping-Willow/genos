import Home from './components/Home.vue';
import Login from './components/auth/Login.vue'

export const routes = [
    {
        path: '/',
        component: Login,
    },
    {
        path: '/home',
        component: Home,
        meta: {
            requireAuth: true
        }
    },
];
