<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_login_with_correct_credentials()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->get('/api/auth/login');

        $this->assertAuthenticatedAs($user);
        $response->assertStatus(200);
    }

    public function test_user_cannot_login_with_wrong_credentials()
    {
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->json('POST', '/api/auth/login', ['email' => 'test@gmail.com','password' => 'test']);

        $response->assertStatus(401);
    }
}
